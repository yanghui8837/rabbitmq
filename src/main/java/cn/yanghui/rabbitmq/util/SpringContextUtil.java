package cn.yanghui.rabbitmq.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class SpringContextUtil implements ApplicationContextAware {
    public static ApplicationContext applicationContext = null;

    public SpringContextUtil() {
    }

    @SuppressWarnings("unchecked")
	public static <T> T getBean(String beanName, Class<T> beanType) {
        Assert.isTrue(applicationContext != null, "应用上下文不能为空");
        Object bean = applicationContext.getBean(beanName);
        return bean == null ? null : (T)bean;
    }

    /**
     * 按类型获取bean
     * @param beanType
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> beanType){
        Assert.isTrue(applicationContext != null, "应用上下文不能为空");
        T bean = applicationContext.getBean(beanType);
        return bean == null ? null : bean;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextUtil.applicationContext = applicationContext;
    }
}
