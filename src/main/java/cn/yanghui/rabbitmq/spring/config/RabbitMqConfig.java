package cn.yanghui.rabbitmq.spring.config;


import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


/**
 * RabbitMq配置
 * @author zhangchenghua
 * @ClassName: RabbitMqConfig
 * @Description: RabbitMq配置
 * @date 2019-03-21 17:15:34
 **/
@Configuration
public class RabbitMqConfig {

    /** 初始化交换机名称 */
    public static final String EXCHANGE_NAME = "datadelivery-E";
    public static final String ROUTING_KEY = "datadelivery-R+id";


    /**
     * MQ配置
     */
    @Value("${spring.rabbitmq.listener.concurrency}")
    private Integer concurrentConsumers;

    @Value("${spring.rabbitmq.listener.max-concurrency}")
    private Integer maxConcurrentConsumers;
    
    /**(@Value("${spring.rabbitmq.host}")
    private String host;
    
    @Value("${spring.rabbitmq.port}")
    private Integer port;*/

    @Value("${spring.rabbitmq.addresses}")
    private String addresses;//MQ地址
    
    @Value("${spring.rabbitmq.username}")
    private String userName;
    
    @Value("${spring.rabbitmq.password}")
    private String password;
    
    @Value("${spring.rabbitmq.virtualHost}")
    private String virtualHost;
    
    @Value("${spring.rabbitmq.connection-timeout}")
    private Integer timeOut;


    
    @Bean(name = "proConnectionFactory")
    @Primary
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		/**connectionFactory.setHost(host);
		connectionFactory.setPort(port);*/
		connectionFactory.setAddresses(addresses);
		connectionFactory.setUsername(userName);
		connectionFactory.setPassword(password);
		if (StringUtils.isNotBlank(virtualHost)) {
			connectionFactory.setVirtualHost(virtualHost);
		}
		
		connectionFactory.setConnectionTimeout(timeOut);
        /**消息确认*/
        connectionFactory.setPublisherConfirms(true);
        /**消息回调*/
        connectionFactory.setPublisherReturns(true);
        /**消费者的ack方式为手动*/



		return connectionFactory;
	}
    
    /**
     * 手动实现监听类配置，采用手动应答模式
     * 解决@RabbitListener监听配置手动应答时的重复应答问题
     * @param connectionFactory
     * @return
     */
  /**  @Bean(name = "simpleRabbitListenerContainerFactory")
    @Primary
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory( ConnectionFactory connectionFactory){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        factory.setConcurrentConsumers(concurrentConsumers);
        factory.setMaxConcurrentConsumers(maxConcurrentConsumers);
        
        return factory;
    }*/



    /**
     * 定义了 AMQP 基础管理操作，主要是对各种资源（交换机、队列、绑定）的申明和删除操作。
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);

        //创建队列和交换机以及绑定

        /**
         * DIRECT
         *
         * direct : 通过路由键 消息将被投送到对应的队列(一对一)
         */
        admin.declareQueue(new Queue("TEST-Queue",true));

        //该交换机里面的三个参数分别为: 名字,持久化,是否自动删除
        admin.declareExchange(new DirectExchange(EXCHANGE_NAME, true, false));

        Binding direct = BindingBuilder.bind(new Queue("TEST-Queue"))
                .to(new DirectExchange(EXCHANGE_NAME, true, false)).with(ROUTING_KEY);
        admin.declareBinding(direct);


        /**
         * FANOUT
         *
         * 发布订阅模式(不存在路由键 将被投放到exchange对应的队列中)
         */
        /**admin.declareQueue(new Queue("Fanout-Queue-1"));
        admin.declareQueue(new Queue("Fanout-Queue-2"));
        admin.declareQueue(new Queue("Fanout-Queue-3"));

        admin.declareExchange(new FanoutExchange("Joe-Fanout", false, false));

        Binding fanout1 = BindingBuilder.bind(new Queue("Fanout-Queue-1"))
                .to(new FanoutExchange("Joe-Fanout", false, false));

        Binding fanout2 = BindingBuilder.bind(new Queue("Fanout-Queue-2"))
                .to(new FanoutExchange("Joe-Fanout", false, false));

        Binding fanout3 = BindingBuilder.bind(new Queue("Fanout-Queue-3"))
                .to(new FanoutExchange("Joe-Fanout", false, false));

        admin.declareBinding(fanout1);
        admin.declareBinding(fanout2);
        admin.declareBinding(fanout3);*/

        /**
         * Topic
         *
         * 可以使得不同源头的数据投放到一个队列中(order.log , order.id, purchase.log, purchase.id)
         *
         * 通过路由键的命名分类来进行筛选
         */
        /**  admin.declareQueue(new Queue("Topic-Queue-1"));
        admin.declareQueue(new Queue("Topic-Queue-2"));
        admin.declareQueue(new Queue("Topic-Queue-3"));

        admin.declareExchange(new TopicExchange("Joe-Topic", false, false));

        Binding topic1 = BindingBuilder.bind(new Queue("Topic-Queue-1"))
                .to(new TopicExchange("Joe-Topic", false, false)).with("*.to");

        Binding topic2 = BindingBuilder.bind(new Queue("Topic-Queue-2"))
                .to(new TopicExchange("Joe-Topic", false, false)).with("log.*");

        Binding topic3 = BindingBuilder.bind(new Queue("Topic-Queue-3"))
                .to(new TopicExchange("Joe-Topic", false, false)).with("log1.to");

        admin.declareBinding(topic1);
        admin.declareBinding(topic2);
        admin.declareBinding(topic3);*/

        return admin;
    }

}


