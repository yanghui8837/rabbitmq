package cn.yanghui.rabbitmq.spring;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author: YangYingHui
 * @create: 2018-05-23 18:04
 **/
@Component
public class Listener2 {

    public static final String EXCHANGE_NAME = "datadelivery-E";
    public static final String QUEUE_NAME = "datadelivery-web-pc"; //表结构变更 创建用户队列
    public static final String ROUTING_KEY = "web-pc";


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = QUEUE_NAME, durable = "true"),
            exchange = @Exchange(
                    value = EXCHANGE_NAME,
                    ignoreDeclarationExceptions = "true",
                    type = ExchangeTypes.TOPIC
                    ,durable = "true"
            ),
            key = {ROUTING_KEY}))
    public void listen(String msg){
        System.out.println("接收到消息：" + msg);

    }





}
