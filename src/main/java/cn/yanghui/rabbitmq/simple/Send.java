package cn.yanghui.rabbitmq.simple;

import cn.yanghui.rabbitmq.util.ConnectionUtil;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * 生产者
 */
public class Send {

//    private final static String QUEUE_NAME = "simple_queue";
//    private final static String QUEUE_NAME = "datadelivery-Q1";
    private final static String QUEUE_NAME = "datadelivery-web-pc";

    public static void main(String[] argv) throws Exception {
        // 获取到连接
        Connection connection = ConnectionUtil.getConnection();
        // 从连接中创建通道，使用通道才能完成消息相关的操作
        Channel channel = connection.createChannel();

        // 声明（创建）队列
        AMQP.Queue.DeclareOk declareOk = channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        // 消息内容
        int messageCount = declareOk.getMessageCount();
        System.out.println("队列内的数据量是： = " + messageCount);
        String message = "Hello World!";
        // 向指定的队列中发送消息
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        
        System.out.println(" [x] Sent '" + message + "'");

        //关闭通道和连接
        channel.close();
        connection.close();
    }
}