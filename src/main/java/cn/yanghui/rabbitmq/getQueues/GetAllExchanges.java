package cn.yanghui.rabbitmq.getQueues;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * @author YangHui
 * @version v1.0
 * @description cn.yanghui.rabbitmq.getQueues
 * @date 2019/12/30 18:00
 */
@Slf4j
public class GetAllExchanges {
    /**
     * 获得CloseableHttpClient对象，通过basic认证。
     * @param username
     * @param password
     * @return
     */
    private static CloseableHttpClient getBasicHttpClient(String username, String password) {
        // 创建HttpClientBuilder
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        // 设置BasicAuth
        CredentialsProvider provider = new BasicCredentialsProvider();
        AuthScope scope = new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM);
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username,password);
        provider.setCredentials(scope, credentials);
        httpClientBuilder.setDefaultCredentialsProvider(provider);
        return httpClientBuilder.build();
    }

    /**
     * 根据API获得相关的MQ信息
     * @param url
     * @param username
     * @param password
     * @return
     */
    private static JSONArray getMQJSONArray(String url, String username, String password) {
        HttpGet httpPost = new HttpGet(url);
        CloseableHttpClient pClient=getBasicHttpClient(username,password);
        CloseableHttpResponse response = null;
        JSONArray jsonArray =null;
        try {
            response = pClient.execute(httpPost);
            StatusLine status = response.getStatusLine();
            int state = status.getStatusCode();
            if (state == HttpStatus.SC_OK) {
                String string = EntityUtils.toString(response.getEntity());
                // 返回来是一个集合，去掉开始的“[”和结尾的“]”;
//                String substring = string.substring(1, string.length() - 1);
                jsonArray = JSONArray.fromObject(string);
            } else {
                System.out.println("请求返回:" + state + "(" + url + ")");
            }
        }catch (Exception e){
            log.error("地址url:"+url+"，异常："+e.toString());
        } finally {
            closeAll(response,pClient);
        }
        return jsonArray;
    }
    private static void closeAll(CloseableHttpResponse response,CloseableHttpClient pClient){
        if (response != null) {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            pClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        String url="http://192.168.202.121:15672/api/exchanges";
        String username="rabbitadm";
        String password="Id49erUi3q";
        JSONArray http = getMQJSONArray(url, username, password);
        System.out.println("所有交换机信息:"+http);
    }

}
