package cn.yanghui.rabbitmq.getQueues.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @author YangHui
 * @version v1.0
 * @description cn.yanghui.rabbitmq.getQueues.util
 * @date 2019/12/30 19:12
 */
@Data
public class BaseResultDTO implements Serializable{
    private static final long serialVersionUID = 3414512748160908650L;
    private boolean success;
    private String message;


}
