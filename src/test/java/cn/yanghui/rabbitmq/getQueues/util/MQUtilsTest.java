package cn.yanghui.rabbitmq.getQueues.util;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author YangHui
 * @version v1.0
 * @description cn.yanghui.rabbitmq.getQueues.util
 * @date 2019/12/30 19:04
 */
public class MQUtilsTest {


    @Test
    public void getMessageCount() {
    }

    @Test
    public void getMessageReadyCount() {
    }

    @Test
    public void getMessagesUnacknowledgedCount() {
    }

    @Test
    public void getMQCountMap() throws IOException {
        MQUtils mqUtils = new MQUtils();
        Map<String, Integer> mqCountMap = mqUtils.getMQCountMap("TEST-Queue");
        System.out.println("mqCountMap = " + mqCountMap);
    }

    @Test
    public void getApiMessage() {
    }
}