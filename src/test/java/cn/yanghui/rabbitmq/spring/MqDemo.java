package cn.yanghui.rabbitmq.spring;

import cn.yanghui.rabbitmq.Application;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;

import static cn.yanghui.rabbitmq.spring.Listener2.QUEUE_NAME;

/**
 * @author: YangYingHui
 * @create: 2018-05-23 18:08
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MqDemo {

    //交换机 datadelivery-E
    private static final  String EXCHANGE_NAME = "datadelivery-E";
    public static final String QUEUE_NAME = "datadelivery-yang"; //表结构变更 创建用户队列
    public static final String ROUTING_KEY = "datadelivery-yang";
    public static final String MQ_QUEUE_PREFIX = "datadelivery-Q";
    public static final String ROUTING_KEY_PREFIX = "datadelivery-R";
    public static final String ENCRYPT_PREFIX = "mysteeldata-R";

    @Autowired
    private RabbitTemplate amqpTemplate;

    @Test
    public void testSend() throws InterruptedException {
        String msg = "hello, Spring boot amqp";
//        List<Integer> msgs = Arrays.asList(1, 2, 3, 3, 5);
//        this.amqpTemplate.convertAndSend(EXCHANGE_NAME,"a.b", msg);
        // 等待10秒后再结束
//        Thread.sleep(10000);


        Connection connection = null;
        Channel channel = null;

        // 获取到连接
        connection = proConnectionFactory.createConnection();
        // 获取通道
        channel = connection.createChannel(false);

        // 声明exchange，指定类型为direct
        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct", true);



            String message = "Hello World!";

         /** 下面这种方式 无法真实查询到队列里有多少条消息 且无法针对某一个队列发送数据  针对某个单独的队列发送消息 还是用simple的send
          *
          * // 声明（创建）队列*/
            AMQP.Queue.DeclareOk declareOk = channel.queueDeclare(QUEUE_NAME, true, false, false, null);
            // 消息内容
            int messageCount = declareOk.getMessageCount();
            System.out.println("队列内的数据量是： = " + messageCount);
            amqpTemplate.convertAndSend(message);

            // 向指定的队列中发送消息
            for (int i = 0; i < 100; i++) {
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes());

//                channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, null, message.getBytes());
            }

            System.out.println(" [x] Sent '" + message + "'");
        } catch (IOException e) {
            e.printStackTrace();
        } /**交换机持久化 */
    }



    @Resource(name = "proConnectionFactory")
    private ConnectionFactory proConnectionFactory;
}
